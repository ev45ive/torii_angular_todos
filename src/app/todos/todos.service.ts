import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class TodosService {

  constructor(private http:HttpClient) { }

  addTodo(name){
    let todo = {
      name, 
      completed: false
    }
    this.http.post(this.url,todo)
    .subscribe(()=>{
      this.fetchTodos()
    })
  }

  private url = 'http://localhost:3000/todos'

  fetchTodos(){
    this.http.get(this.url)
    .subscribe( (todos:any) => this.todos$.next(todos))
  }
  private todos$ = new BehaviorSubject([])


  searchTodos(query){
    return this.http.get(
      this.url + '?q=' + query
    )
  }


  getTodos(){
    this.fetchTodos()
    return this.todos$.asObservable()
  }

}
