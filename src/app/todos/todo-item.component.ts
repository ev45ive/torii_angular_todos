import { Component, OnInit, Input, HostBinding, Inject } from '@angular/core';

@Component({
  // <todo-item> lub <li todo></li>
  selector: 'todo-item, [todo-item]',
  template: `
        <input type="checkbox" [checked]="todo.completed"
                               (change)="toggleCompleted(todo)"> 

      <span *ngIf="!editting" (click)="editting = true">{{todo.name}} </span>


      <input *ngIf="editting"
                [(ngModel)]="todo.name"
                (keyup.esc)="editting = false"
                (keyup.enter)="editting = false">
  `,
  styles: [`
    :host(.todo-completed){
      text-decoration:line-through;
    }
  `]
})
export class TodoItemComponent implements OnInit {

  @HostBinding('class.todo-completed')
  get isTodoCompleted(){
    return this.todo['completed'];
  }

  @Input('value')
  todo = {}

  toggleCompleted(todo){
    todo.completed = !todo.completed
  }

  editting = false;

  constructor(@Inject('counter') private counter) {
    //console.log(counter)
  }

  ngOnInit() {
  }

}
