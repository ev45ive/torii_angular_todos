import { Component, OnInit, Input, ContentChild } from '@angular/core';
import { NgModel } from "@angular/forms";
let count = 0;

export function CounterFactory(){
  count++;
  return count;
}

@Component({
  selector: 'todos-list',
  template: `
    <ng-content select=".list-header"></ng-content>
     <ul>
      <li 
        *ngFor="let todo of todos" 
        todo-item
        [value]="todo" 
        class="list-group-item"></li>
    </ul>

    <ng-content></ng-content>
  `,
  providers:[
    {provide: 'counter', useFactory: CounterFactory}
  ],
  styles: [`
    :host(){
      border:2px solid blue;
      display:block;
      padding:1em;
    }
  `]
})
export class TodosListComponent implements OnInit {

  @ContentChild(NgModel,{read:NgModel})
  itemRef

  ngAfterContentInit(){
    console.log(this.itemRef)
  }

  @Input()
  todos

  constructor() { }

  ngOnInit() {
  }

}
