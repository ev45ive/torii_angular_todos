import { Component, OnInit } from '@angular/core';
import { TodosService } from "./todos.service";

@Component({
  selector: 'todos',
  template: `

    <todos-list [todos]="todos$ | async">
      
      <h3 class="list-header">Todos</h3>
      
      <div class="list-footer">
        <input type="text"  #itemRef
                class="form-control"
              [(ngModel)]="name"
              (keyup.enter)="addTodo()">
      </div>
    
    </todos-list>

  `,
  styles:[]
})
export class TodosComponent implements OnInit {

  name = ''

  addTodo(){
    this.service.addTodo(this.name)
    this.name = ''
  }

  todos = [ ]


  constructor(private service:TodosService) { }

  todos$

  ngOnInit() {
    this.todos$ = this.service.getTodos()
  }

}
