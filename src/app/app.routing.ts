import { RouterModule, Routes } from '@angular/router'
import { UsersComponent } from "./users/users.component";
import { SearchComponent } from "./search/search.component";
import { TodosComponent } from "./todos/todos.component";
import { UserContainerComponent } from "./users/user-container.component";

const routes:Routes = [
    {path:'', redirectTo:'users', pathMatch:'full' },
    {path:'search', component: SearchComponent },
    {path:'users', component: UsersComponent, children:[
        { path:'', component: UserContainerComponent},
        { path:'show/:id', component: UserContainerComponent}
    ]  },
    {path:'todos', component: TodosComponent},
    // {path:'todos/:id', component: TodosComponent },
    {path:'**', redirectTo:'search', pathMatch:'full' }
]

export const Routing = RouterModule.forRoot(routes,{
    enableTracing: true,
    useHash: true
})