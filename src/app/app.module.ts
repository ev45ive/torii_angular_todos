import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import { TodoItemComponent } from './todos/todo-item.component';
import { UsersComponent } from './users/users.component';
import { UserRegisterComponent } from './users/user-register.component';
import { UsersListComponent } from './users/users-list.component';
import { UsersService } from "./users/users.service";
import { HttpClientModule } from '@angular/common/http'
import { TodosService } from "./todos/todos.service";
import { SearchComponent } from './search/search.component';
import { SearchFormComponent } from './search/search-form.component';
import { SearchResultsComponent } from './search/search-results.component';
import { TodosListComponent } from "./todos/todos-list.component";

import { Routing } from './app.routing';
import { UserContainerComponent } from './users/user-container.component'

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    TodosListComponent,
    TodoItemComponent,
    UsersComponent,
    UserRegisterComponent,
    UsersListComponent,
    SearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    UserContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Routing
  ],
  providers: [
    UsersService,
    TodosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
