import { Component, OnInit } from '@angular/core';
import { TodosService } from "../todos/todos.service";

@Component({
  selector: 'search',
  template: `    
    <div class="row">
      <div class="col">
        <search-form (search)="search($event)"></search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <button (click)="show=!show">Toggle</button>
        
        <search-results *ngIf="!show" [results]="todos$ | async"></search-results>
      </div>
    </div>
  `,
  styles: [`
    ::ng-deep h1{
      color:blue !important;
    }
  `]
})
export class SearchComponent implements OnInit {

  todos$

  search(query){
    this.todos$ = this.service.searchTodos( query )
  
  }

  constructor(private service:TodosService) { }

  ngOnInit() {
  }

}
