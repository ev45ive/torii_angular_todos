import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup,  AbstractControl, FormControl, FormArray } from '@angular/forms'

// import 'rxjs/Rx'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/debounceTime'
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'search-form',
  template: `
    <form [formGroup]="queryForm">
      <div class="input-group mt-4 mb-4">
        <input class="form-control" #queryRef formControlName="search_query">

        <span class="input-group-btn">
          <button class="btn btn-secondary" (click)="search(queryRef.value)">Go!</button>
        </span>
      </div>
    </form>
  `,
  styles: []
})
export class SearchFormComponent implements OnInit {

  queryForm:FormGroup

  // http://reactivex.io/rxjs/

  constructor() {
    this.queryForm = new FormGroup({
      'search_query': new FormControl('placki')
    })
    //console.log(this.queryForm)
    window['queryForm'] = this.queryForm;

    this.query = this.queryForm
      .get('search_query')
      .valueChanges
      //.do(q => console.log('q',q))
      .debounceTime(400)
      .filter( query => query.length >= 3)
      .distinctUntilChanged()
      //.subscribe( query => this.query.next(query) )
  }

  @Output('search')
  query
  
  ngOnInit() {
  }

  //query = ''


}
