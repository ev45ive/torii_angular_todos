import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'search-results',
  template: `
   <todos-list [todos]="results"></todos-list>
  `,
  styles: []
})
export class SearchResultsComponent implements OnInit {

  @Input()
  results = []

  constructor() { }

  ngOnInit() {
  }

}
