import { Component, OnInit } from '@angular/core';
import { UsersService } from "./users.service";
import { ActivatedRoute } from '@angular/router'

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'

@Component({
  selector: 'user-container',
  template: `
      <user-register [user]="user$ | async" (save)="newUser($event)"></user-register>
  `,
  styles: []
})
export class UserContainerComponent implements OnInit {

  newUser(user){
    this.service.registerUser(user)
  }
  
  constructor(private service:UsersService, private route:ActivatedRoute) {}

  user$

  ngOnInit() {    
    // let id = this.route.snapshot.params['id']

    
    this.user$ =this.route.params
    .map( params => params['id'])
    .switchMap(id  => this.service.fetchUser(id))

  }

}
