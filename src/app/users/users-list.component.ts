import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'users-list',
  template: `
 <table class="table">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Email</th>
    </tr>
  </thead>
  <tbody>
    <tr *ngFor="let user of users; let i = index" [routerLink]="['show',user.id]">
      <th scope="row">{{ i + 1 }}.</th>
      <td>{{user.name}}</td>
      <td>{{user.email}}</td>
      <td (click)="remove(user)"> &times; </td>
    </tr>
  </tbody>
  </table>
  `,
  styles: []
})
export class UsersListComponent implements OnInit {

  remove(user){
    let index = this.users.indexOf(user)
    this.users.splice(index,1)
  }

  @Input('value') 
  users = []

  constructor() { }

  ngOnInit() {
  }

}
