import { Component, OnInit, Inject } from '@angular/core';
import { UsersService } from './users.service'
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'users',
  template: `
  <div class="row">
    <div class="col">
      <router-outlet></router-outlet>
    </div>
    <div class="col">
      <button (click)="refresh()">Refresh</button>
      <users-list [value]="users$ | async "></users-list>
    </div>
  </div>
  `,
  styles: [`
    ::ng-deep h1{
      color:red !important;
    }
    :host(){
      border:1px solid black;
      display:block;
    }

    :host-context(.sidebar){
      background:red;
    }


    :host ::ng-deep th{
      color: rebeccapurple;
    }
  `]
})
export class UsersComponent implements OnInit {

  newUser(user){
    this.service.registerUser(user)
  }
  
  refresh(){
    this.service.fetchUsers()
  }

  constructor(private service:UsersService) {}

  users$:Observable<any>

  ngOnInit() {
    this.users$ = this.service.getUsers()
  }

}
