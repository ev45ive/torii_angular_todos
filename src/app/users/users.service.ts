import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
// import { Observable } from 'rxjs/Observable'
// import { Subject } from 'rxjs/Subject'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

// import 'rxjs/add/operator/startWith';

@Injectable()
export class UsersService {

  constructor(private http:HttpClient) {
    console.log(this)
  }
  
  registerUser(user){
    this.http.post(this.url,user)
    .subscribe( user => {
      this.fetchUsers()
    })
  }

  private url = 'http://localhost:3000/users/';

  fetchUser(id){
    return this.http.get(this.url + id)
  }

  fetchUsers(){
    this.http.get(this.url)
    .subscribe( (users:any) => this.users$.next(users) )
  }

  users$ = new BehaviorSubject<any[]>([])

  getUsers(){
    return this.users$.asObservable()
  }


}
