import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'user-register',
  template: `
  <div class="card">
    <div class="card-body">
    <h4 class="card-title">User Registration</h4>
    <form #formRef="ngForm" (submit)="register(formRef)">
        <input type="hidden" ngModel name="id">
      <div class="form-group">
        <label>Your name</label>
        <input type="text" ngModel name="name" class="form-control" required minlength="3" placeholder="Enter name">
        <span *ngIf="formRef.controls.name?.touched  || formRef.controls.name?.dirty">
          <small *ngIf="formRef.controls.name?.errors?.required" class="form-text text-muted">Field is required</small>
          <small *ngIf="formRef.controls.name?.errors?.minlength" class="form-text text-muted">Value too short</small>
        </span>
      </div>
      <div class="form-group">
        <label>Email address</label>
        <input type="email" email ngModel name="email" class="form-control" placeholder="Enter email">
         <span *ngIf="formRef.controls.email?.touched  || formRef.controls.email?.dirty">
          <small *ngIf="formRef.controls.email?.errors?.required" class="form-text text-muted">Field is required</small>
          <small *ngIf="formRef.controls.email?.errors?.email" class="form-text text-muted">Invalid email format</small>
        </span>
      </div>
      <div class="form-group" *ngIf="user.password">
        <label>Password</label>
        <input type="password" ngModel name="password" class="form-control"  required minlength="3"  placeholder="Password">
        <span *ngIf="formRef.controls.password?.touched  || formRef.controls.password?.dirty">
          <small *ngIf="formRef.controls.password?.errors?.required" class="form-text text-muted">Field is required</small>
          <small *ngIf="formRef.controls.password?.errors?.minlength" class="form-text text-muted">Value too short</small>
        </span>
      </div>
      <div class="form-check" hidden>
        <label class="form-check-label">
          <input type="checkbox" class="form-check-input">
          Check me out
        </label>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
  </div>
  `,
  styles: [`
    input.ng-invalid.ng-touched,
    input.ng-invalid.ng-dirty {
      border: 2px solid red;
    }
  `]
})
export class UserRegisterComponent implements OnInit {

  @ViewChild('formRef')
  formRef

  register(form) {
    if(form.invalid){
      return;
    }
    let user = form.value;
    this.savedUserEmitter.emit(user)
    form.reset()
  }

  @Input('user')
  set setUser(user){
    if(!user){ return }
    
    this.formRef.setValue(user)
  }


  user = {}

  // @Output, angular robi  = this.savedUserEmitter.subscribe($event => ...)

  @Output('save')
  savedUserEmitter = new EventEmitter()


  constructor() { }

  ngOnInit() {
  }

}
